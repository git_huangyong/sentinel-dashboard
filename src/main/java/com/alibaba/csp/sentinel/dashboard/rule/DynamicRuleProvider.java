
package com.alibaba.csp.sentinel.dashboard.rule;

/**
 * @author Eric Zhao
 * @since 1.4.0
 */
public interface DynamicRuleProvider<T> {
    
    default T getRules(String appName) throws Exception{
        return null;
    };
    
    default T getRules(String appName,String ip,Integer port) throws Exception{
        return null;
    };
}
